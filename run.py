#!flask/bin/python
from app.server import app
from config import  *

app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['ALLOWED_EXTENSIONS'] = set(['mp4','jpg'])
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

app.run(debug = True, host= '0.0.0.0')
