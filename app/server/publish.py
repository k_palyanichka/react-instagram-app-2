import logging
import os
from datetime import datetime

import pandas as pd
import requests
import sqlalchemy
from moviepy.editor import VideoFileClip

from app.server.lib.instagram_private_api import Client
from app.server.lib.InstagramAPI import InstagramAPI
from config import SQLALCHEMY_DATABASE_URI


import redis
import pickle

r = redis.StrictRedis('127.0.0.1')



logging.basicConfig(format=u'[%(asctime)s] %(levelname)-8s %(filename)s[LINE:%(lineno)d]#  %(message)s',
                    level=logging.DEBUG,
                    #filename='./log/app.log'
                    )

engine = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)

#username, password= 'fun_profun','Mari1990*'
def login(username, password):
    api = InstagramAPI(username, password)
    api.login()
    r.set(username+"-activity", pickle.dumps(api))
    return api

def checkLogin(username, password, type="search"):
    if type =="search":
        try:
            logging.info('loading data from redis')
            res= r.get(username+"-activity")
            api = pickle.loads(res)
        except:
            logging.info('data in redis not found: using login func')
            api= login(username, password)
        return api
    else:
        try:
            logging.info('loading data from redis')
            res= r.get(username+ "-post")
            api = pickle.loads(res)
        except:
            logging.info('data in redis not found: using login func')
            api = Client(username, password)
            #r.set(username + "-post", pickle.dumps(api))
        return api


def download_file(url):
    name = url.split("/")[-1].split("?")[0]
    type = name.split(".")[-1]
    path = './app/server/tmp/photos/'+name if type =='jpg' else  './app/server/tmp/videos/'+name
    response = requests.get(url, stream=True)
    with open( path, 'wb') as f:
            f.write(response.content)
    return path



def get_video_content(items):
    data = []
    for i in range(len(items)):
        if 'video_versions' in items[i].keys():
            data += [[items[i]['caption']['media_id'],
                    items[i]['caption']['created_at'],
                    items[i]['caption']['text'],
                    items[i]['caption']['user']['pk'],
                    items[i]['caption']['user']['username'],
                    items[i]['comment_count'] if 'comment_count' in items[i].keys() else 0,
                    items[i]['like_count'] if 'like_count' in items[i].keys() else 0,
                    items[i]['video_duration'] if 'video_duration' in items[i].keys() else 0,
                    items[i]['view_count'] if 'view_count' in items[i].keys() else 0,
                    items[i]['video_versions'][0]['url'],
                    items[i]['image_versions2']['candidates'][0]['url']
                    ]]
    return data

def to_database(items , engine , user):
    data = pd.DataFrame(items)
    data.columns = ['media_id', 'created_at', 'text', 'user_id', 'user_name', 'comment_count',
           'like_count', 'video_duration', 'view_count', 'video_url',
           'video_image']
    data['inserted_at'] = datetime.now()
    data['created_at'] = data['created_at'].map(lambda x: datetime.fromtimestamp(x))
    data['user_add'] = user
    for i in range(len(data)):
        try:
            data.iloc[i:i+1].to_sql(name="items", con= engine, if_exists="append", index=False)
        except Exception as e: logging.error("can not insert data to items table "+str(e))


def publish(engine,  media_id, username, password,  tags = '#fun #funny'):
    try:
        #api = Client(username , password)
        api = checkLogin(username, password, type="post")
        data = list(engine.execute("select video_url, video_image from items where media_id = {}".format(media_id)))
        thumbnail_local_path=download_file(data[0][1])
        video_local_path = download_file(data[0][0])
        #thumbnail_local_path="./app/server/tmp/photos/videos.locos_30_3_2017_12_39_18_817.jpg"
        #video_local_path = "./app/server/tmp/videos/17501682_789575487866595_1471677480346583040_n.mp4"
        videoData = open(video_local_path, 'rb').read()
        imageData = open(thumbnail_local_path, 'rb').read()
        clip = VideoFileClip(video_local_path)
        print((clip.w, clip.h),int(round(clip.duration,0)) )
        try:
            api.post_video(video_data=videoData, thumbnail_data=imageData, size=(clip.w, clip.h), duration=clip.duration, caption=tags)
            pd.DataFrame({'media_id': media_id, 'inserted_at': datetime.now(), 'text': tags},
                         index=[0]).to_sql(name="posted_items", con=engine, if_exists="append", index=False)
        except Exception as e: logging.error("Video not loaded: "+str(e))
        try:
            del clip
            del videoData
            os.remove(thumbnail_local_path)
            os.remove(video_local_path)
        except Exception as e :
            logging.error("can not remove files:  " + str(e))
    except Exception as e:logging.error(str(e))


def publish_file(api, tags, videoFilePath, imageFilePath):
    api.uploadVideo(video=videoFilePath, thumbnail=imageFilePath , caption=tags)
    os.remove(videoFilePath)
    os.remove(imageFilePath)


def add_to_tracking(name, engine,user_id, username , password, type="name"):
    api = checkLogin(username,  password)
    if type =="name":
        api.searchUsername(name)
        user = api.LastJson['user']
        try:
            engine.execute("""INSERT INTO i_users
                            (id,  full_name, biography, username, media_count, follower_count, following_count,
                            geo_media_count, usertags_count, profile_pic_url, is_business, is_favorite, is_private,
                             is_verified,  has_chaining, user_add)
                             VALUES
                             ({id},'{full_name}', '{biography}','{username}',{media_count},{follower_count},{following_count},
                              {geo_media_count},{usertags_count},'{profile_pic_url}','{is_business}','{is_favorite}','{is_private}',
                              '{is_verified}', '{has_chaining}', {user_add} )""".format(id = user['pk'], full_name=user['full_name'],biography=user['biography'],
                                                                        username=user['username'],  media_count=user['media_count'],
                                                                        follower_count=user['follower_count'], following_count=user['following_count'],
                                                                        geo_media_count= user['geo_media_count'] if 'geo_media_count' in user.keys() else 'Null',
                                                                        usertags_count=user['usertags_count'],
                                                                        profile_pic_url=user['profile_pic_url'], is_business=user['is_business'],
                                                                        is_favorite=user['is_favorite'], is_private=user['is_private'],
                                                                        is_verified=user['is_verified'],has_chaining=user['has_chaining'], user_add=user_id))
        except Exception as e: logging.error("can not insert into i_users table: "+str(e))
        try:
            engine.execute("""INSERT INTO to_watch
              (item_id, name, user_add, inserted_at )
            VAlUES
            ({id},'{username}', {user_add}, '{inserted_at}')
            """.format(id =  user['pk'], username=user['username'], user_add=user_id , inserted_at= datetime.now()))
        except Exception as e:logging.error("can not insert into to_watch table: "+str(e))
    else:
        engine.execute("""INSERT INTO i_tag
                        (tag_name, user_add, cr_date)
                        VALUES
                        ('{tag_name}', {user_add}, '{cr_date}')""".format(tag_name = name,
                                                                      user_add =user_id,
                                                                      cr_date = datetime.now()))
        try:
            engine.execute("""INSERT INTO to_watch
                         ( name, user_add, inserted_at )
                       VAlUES
                       ('{username}', {user_add}, '{inserted_at}')
                       """.format(username=name, user_add=user_id, inserted_at=datetime.now()))
        except Exception as e: logging.error("can not insert into to_watch table: "+str(e))




def get_content_by_name(user_id, user, username, password):
    api = checkLogin(username, password)
    try:
        user_id= engine.execute("select id from i_users where username = '%s'"%(user_id) ).fetchall()[0].values()[0]
    except Exception as e:logging.error("can not get data from i_user table: "+str(e))

    api.SendRequest("feed/user/"+str(user_id))
    try:
        items = get_video_content(api.LastJson['items'])
    except Exception as e :
        logging.error("can not get video content by name: "+str(api.LastJson))
    to_database(items, engine, user)


def get_content_by_tag(tag, user, username, password):
    try:
        try:
            api = checkLogin(username, password)
        except Exception as e:logging.error("not loggined: "+str(e))
        logging.debug("get tag: "+ tag)
        lastId = ""
        for i in range(10):
            try:
                api.getHashtagFeed(tag.replace("#", ""), lastId)
                lastId = api.LastJson['next_max_id'].replace("%3D", "")
                items = get_video_content(api.LastJson['items'])
                to_database(items, engine, user)
            except Exception as e: logging.error(str(e))
    except Exception as e: logging.error("error from get_content_by_tag: "+str(e))


# data= pd.DataFrame()
# lastId = ""
#
# for i in range(50):
#     api.getHashtagFeed(tag.replace("#", ""), lastId)
#     lastId = api.LastJson['next_max_id'].replace("%3D", "")
#     items = get_video_content(api.LastJson['items'])
#     data = data.append(pd.DataFrame(items))
