import pika

def sendTask(message):
    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='127.0.0.1'))
    channel = connection.channel()

    channel.queue_declare(queue='instagram_bot', durable=True)

    channel.basic_publish(exchange='',
                          routing_key='instagram_bot',
                          body=message,
                          properties=pika.BasicProperties(
                              delivery_mode = 2, # make message persistent
                                ))
    connection.close()

for i in range(10):
    sendTask("Hello")