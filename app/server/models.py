from app.server import db

ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    password  = db.Column(db.String(64))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id


    def __repr__(self):
        return '<User %r>' % (self.nickname)

class Items(db.Model):
    __tablename__ = 'items'
    media_id = db.Column(db.BigInteger, primary_key = True)
    created_at = db.Column(db.DateTime)
    text = db.Column(db.Text, nullable=True)
    user_id = db.Column(db.BigInteger)
    user_name = db.Column(db.String)
    comment_count = db.Column(db.Integer, nullable=True)
    like_count = db.Column(db.Integer, nullable=True)
    video_duration = db.Column(db.Numeric, nullable=True)
    view_count = db.Column(db.Integer, nullable=True)
    video_url = db.Column(db.String, nullable=True)
    video_image = db.Column(db.String, nullable=True)
    inserted_at = db.Column(db.DateTime, nullable=True)
    inserted_user = db.column(db.Integer, db.ForeignKey('users.id'))
