from flask.ext.wtf import Form
from sqlalchemy import create_engine
from wtforms import StringField, PasswordField, validators

from app.server.models import User
from config import *

engine = create_engine(SQLALCHEMY_DATABASE_URI)


class LoginForm(Form):
    username = StringField('Username', [validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        print(self.username.data+" "+self.password.data)
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(
            email=self.username.data.lower()).first()

        if user is None:
            self.username.errors.append('Unknown username')
            return False

        if  user.password != self.password.data:
            self.password.errors.append('Invalid password')
            return False

        self.user = user
        return True


class SingUpForm(Form):
    username = StringField('Username', [validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])
    email = StringField('Username', [validators.DataRequired()])
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        print(self.username.data+" "+self.password.data)
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(
            nickname=self.username.data.lower()).first()

        if user is not None:
            self.username.errors.append('Имя пользователя занято')
            return False

        elif  len(self.password.data)<=5:
            self.password.errors.append('Длинна пароля должна быть более 5 символов')
            return False

        engine.execute("""INSERT INTO users (nickname, email, role, password )
                       values('{}', '{}', 1, '{}')""".format(self.username.data, self.email.data, self.password.data))


        self.user = user
        return True
