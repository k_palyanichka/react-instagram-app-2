import codecs
import json
from datetime import *

from app.server.models import User
from app.server.publish import *
from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import logout_user, current_user, login_required

from app.server import app, lm
from app.server.forms import LoginForm, SingUpForm
import sqlalchemy
import logging
from config import *

engine = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)

logging.basicConfig(format=u'[%(asctime)s] %(levelname)-8s %(filename)s[LINE:%(lineno)d]#  %(message)s',
                    level=logging.INFO,
                    #filename='./log/app.log'
                    )

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=60)

@app.route('/api/check_acc', methods=['GET'])
@login_required
def checkAcc():
    cnt_acc =engine.execute("""select * from users u
                      inner join insta_acc ia on u.id =ia.user_add
                      where u.id = {}""".format(g.user.id)).rowcount
    return json.dumps({'cnt_acc':cnt_acc})

@app.route('/api/add_acc', methods=['POST'])
@login_required
def addAcc():
    data = json.loads(codecs.decode(request.data))
    logging.info(str(data))
    password = data["password"]
    username = data["username"]
    cnt_acc =engine.execute("""INSERT INTO insta_acc
                                (user_add, i_login, i_pass, inserted_at, is_active)
                                VALUES({user_add}, '{i_login}', '{i_pass}', '{inserted_at}', 'true')""".format(user_add=g.user.id,
                                                                                                          inserted_at=datetime.now(),
                                                                                                          i_pass=password,
                                                                                                          i_login = username))
    return json.dumps({'success': True}), 200



@app.route('/')
@app.route('/exchange')
@login_required
def index():
    user = g.user
    return render_template('index3.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        flash(u'Successfully logged in as %s' % form.user.email)
        session['user_id'] = form.user.id
        return redirect(url_for('index'))
    return render_template('login.html',
                           title='Sign In',
                           form=form
                           )

@app.route('/singup', methods=['GET', 'POST'])
def singup():

    form = SingUpForm()
    if form.validate_on_submit():
        #flash(u'Successfully logged in as %s' % form.user.email)
        #session['user_id'] = form.user.id
        return redirect(url_for('login'))

    return render_template('singup.html',
                           title='Sign In',
                           form=form
                           )




@app.route('/api/publish' , methods=['POST'])
@login_required
def publish_item():
    #print(request.data)
    data = json.loads(codecs.decode(request.data))
    media_id= data['media_id']
    text = data['tags']
    logging.info(str(data))
    logging.info("Publish: "+ str(media_id)+" "+" "+str(text))
    d= engine.execute("select i_login, i_pass from insta_acc where user_add = {}".format(g.user.id)).fetchall()
    publish(engine,int(media_id), username=d[0]['i_login'], password=d[0]['i_pass'] , tags=text)


@app.route('/hide/<int:media_id>' , methods=['GET'])
@login_required
def hide_item(media_id):
    try:
        engine.execute("INSERT INTO hided_items VALUES (%d, '%s' )" %(media_id, datetime.now()))
        return json.dumps({'success': True}), 200
    except Exception as e :
        logging.error("can not hide item: {media_id}".format(media_id))
        return json.dumps({'success': False}), 500



@app.route('/upload', methods=['GET', 'POST'] )
@login_required
def upload():
    MAX_FILE_SIZE = 10*1024 * 1024 +1
    if request.method =="GET":
        return render_template('upload.html',
                               title='Загрузить видео'
                               )
    if request.method == "POST":
        videoFile = request.files["video-file"]
        imageFile = request.files["image-file"]
        if bool(videoFile.filename):
            video_bytes = videoFile.read(MAX_FILE_SIZE)
            image_bytes = imageFile.read(MAX_FILE_SIZE)
        tags = request.form['tags']
        ##gym #fun #funny #ржач

        videoFilePath, imageFilePath ='./tmp/videos/'+ videoFile.filename, './tmp/photos/'+ imageFile.filename
        with open(videoFilePath ,'wb') as f:
            f.write(video_bytes)
        videoFile.close()
        with open(imageFilePath ,'wb') as f:
            f.write(image_bytes)
        imageFile.close()

        api = login()
        try:
            publish_file(api, tags, videoFilePath, imageFilePath)
        except Exception as e:logging.error("can not upload single file: "+str(e))
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

#передаем username или tag как параметр запроса
@app.route('/api/update_content', methods=['GET', 'POST'])
@login_required
def update_content():
    try:
        name = json.loads( codecs.decode(request.data))['name']
        logging.info('update content by: '+name)
        d = engine.execute("select i_login, i_pass from insta_acc where user_add = {}".format(g.user.id)).fetchall()
        if "#" not in name:
            get_content_by_name(name, g.user.id, username=d[0]['i_login'], password=d[0]['i_pass'])
        if "#"  in name:
            get_content_by_tag(name,  g.user.id, username=d[0]['i_login'], password=d[0]['i_pass'])
        return json.dumps({'success': True}), 200
    except Exception as e:
        logging.error("can not udate content: "+str(e))
        return json.dumps({'success': False}), 500


@app.route('/account')
@login_required
def account():
    return render_template('account.html')

#TODO: mediaid должен быть как index в бд чтобы небыло дублей на странице
@app.route('/api/list_items', methods=['GET'])
@login_required
def items_list():
    limit =3
    page = request.args.get('page')
    logging.info(page)
    offset = (int(page)+1) *limit
    logging.info("update date by offset "+ str(offset))
    data = pd.read_sql("select * from v_items where user_add = {user_add} limit {limit} offset {offset}".format(user_add=g.user.id, limit=limit, offset=offset), engine)
    data['media_id'] = data['media_id'].astype(str)
    return data.to_json(orient="index")


@app.route('/api/tracking', methods=['POST', 'GET'])
@login_required
def add_tracking():
    try:
        #print(request.data)
        data = json.loads( codecs.decode(request.data))
        d = engine.execute("select i_login, i_pass from insta_acc where user_add = {}".format(g.user.id)).fetchall()

        if data['username'].strip()[0]!="#":
            name = data['username']
            logging.debug(name+ " -name to tracking")
            add_to_tracking(name, engine,  username=d[0]['i_login'], password=d[0]['i_pass'],  user_id= g.user.id, type="name")
        if  data['username'].strip()[0]=="#":
            name =  data['username']
            logging.debug(name+ " -tag to tracking")
            add_to_tracking(name, engine, username=d[0]['i_login'], password=d[0]['i_pass'],  user_id= g.user.id,  type="tag")
        return json.dumps({'success': True}), 200
    except Exception as e:
        logging.error(str(e))
        return json.dumps({'success': False}), 500


@app.route('/api/tracking_list', methods=['POST', 'GET'])
@login_required
def tracking():
    if request.method =="GET":
        tracking_list = pd.read_sql("""select id, name, user_add from public.to_watch
                                        where is_active =1 and user_add = {user_add}""".format(user_add=g.user.id), engine)
        return tracking_list.to_json(orient="index")

@app.route('/api/delete_from_tracking', methods=['GET'])
@login_required
def delete_from_tracking():
    try:
        id = request.args.get("id")
        engine.execute('UPDATE to_watch set is_active =0 where id = {id} and user_add= {user_add}'.format(user_add=g.user.id, id = id))
        return json.dumps({'success': True}), 200
    except Exception as e:
        logging.error("can not delete item from tracking: {id}".format(id))
        return json.dumps({'success':False}), 500


@app.route('/api/exchange/addoffer', methods=['POST'])
def add_offer():
    data = json.loads( codecs.decode(request.data))["data"]
    print(data)
    engine.execute("""
    INSERT INTO offers (name, price, subject, link, followers , likes, views, manager, manager_link,  description , cr_date, user_id )
    VALUES('{name}', '{price}','{subject}', '{link}', {followers}, {likes}, {views}, '{manager}', '{manager_link}',  '{description}' ,  '{cr_date}', {user_id})""".format(
        subject=data['subject'],
        link=data['link'],
        followers=data['followers'],
        likes=data['likes'],
        views=data['views'],
        manager=data['manager'],
        manager_link=data['manager_link'],
        description=data['description'],
        cr_date=datetime.now(),
        user_id = g.user.id,
        name = data['name'],
        price = data['price'],
    ))
    print("data to offer: ", data)

@app.route('/api/exchange/getoffers')
def get_offers():
    data = pd.read_sql("select * from offers", engine)
    return data.to_json(orient="index")