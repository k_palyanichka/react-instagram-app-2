import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './common/Header';
import * as GlobalActions from './common/GlobalActions';
import ClassNames from 'classnames';


const mapStateToProps = (state)=> {
    return {
        fetchesInProgress: state.globalData.fetchesInProgress
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        globalActions: bindActionCreators(GlobalActions, dispatch)
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export default class extends Component {

    render() {
        let ajaxLoaderClass = ClassNames('wait', {'hidden': !this.props.fetchesInProgress.length});

        return (
            <div>
                <Header />
                <div className="content-wrapper">
                    <div className="row">
                        <div className="span12">
                            <div className="content">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>

                <div className={ajaxLoaderClass}>
                    <div>
                        <h6>Please wait...</h6>
                        <img src={require("../img/ajax-loader.gif")} alt="loading..."/>
                    </div>
                </div>
            </div>
        )
    }
}