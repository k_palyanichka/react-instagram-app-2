import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import GlobalReducer from "./GlobalReducer";
import VideoReducer from "../gallery/reducers/VideoReducer";
import TrackListReducer from "../gallery/reducers/TrackListReducer";
import CheckAccReducer from "../gallery/reducers/CheckAccReducer";
import SearchFormReducer from "../gallery/reducers/SearchFormReducer";
import ExchangeReducer from "../exchange/reducers/ExchangeReducer";
import SetFormReducer from "../exchange/reducers/SetFormReducer";
import SortResultsReducer from "../gallery/reducers/SortResultsReducer";

export default combineReducers({
    routing: routerReducer,
    globalData: GlobalReducer,
    videoData: VideoReducer,
    trackListData:TrackListReducer,
    checkAccData:CheckAccReducer,
    searchFormData:SearchFormReducer,
    exchangeData:ExchangeReducer,
    setFormData:SetFormReducer,
    sortResultsData:SortResultsReducer,
});