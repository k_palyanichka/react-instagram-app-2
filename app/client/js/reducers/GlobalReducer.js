
import {FETCH_STARTED, FETCH_FINISHED} from "../common/GlobalActions";
import _ from "lodash";

const initialState = {
    fetchesInProgress: []
};

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_STARTED:
            let fetches = state.fetchesInProgress.concat([action.payload]);
            return {...state, fetchesInProgress: fetches};
        case FETCH_FINISHED:
            let filtered = _.filter(state.fetchesInProgress, function(e) {
                return e !== action.payload;
            });
            return {...state, fetchesInProgress: filtered};
    }
    return state
}