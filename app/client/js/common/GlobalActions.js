export const FETCH_STARTED = 'FETCH_STARTED';
export const FETCH_FINISHED = 'FETCH_FINISHED';


export function fetchStarted(url) {
    return {
        type: FETCH_STARTED,
        payload: url
    }
}

export function fetchFinished(url) {
    return {
        type: FETCH_FINISHED,
        payload: url
    }
}



