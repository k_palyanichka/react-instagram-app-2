

import React, { Component } from 'react';
export default class extends Component {
    render() {
        return (
            <div >
                <ul id="dropdown1" className="dropdown-content">
                      <li><a href="/video/upload">Загрузить видео</a></li>
                  </ul>
                    <ul id="dropdown2" className="dropdown-content">
                      <li><a href="/bot">Бот</a></li>
                      <li><a href="/exchange">Биржа рекламы</a></li>
                  </ul>
                <nav>
                  <div className="nav-wrapper teal lighten-2">
                    <a href="/" className="brand-logo">Home Page</a>
                    <ul className="right hide-on-med-and-down">
                      <li><a href="/account">Аккаунт</a></li>
                      <li><a href="/logout">Выход</a></li>
                      <li><a className="dropdown-button" href="#!" data-activates="dropdown1">Действия<i className="material-icons right">arrow_drop_down</i></a></li>
                      <li><a className="dropdown-button" href="#!" data-activates="dropdown2">Продвижение<i className="material-icons right">arrow_drop_down</i></a></li>
                    </ul>
                  </div>
                </nav>
            </div>
        );
    }
}