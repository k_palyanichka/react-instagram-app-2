import React, { Component } from 'react';
import Input from 'components/input/Input';
export default class extends Component {

    render() {
        const {onSubmit, onChange} = this.props;


        //TODO дать возможность настройки поиска
        return (
            <div>
                <div className="col s3">
                    <Input
                        value = {this.props.SearchValue}
                        placeholder="если первый знак # ищем по тэгам"
                        onChange={this.props.onChange}
                    />
                </div>
                <div className="col s1">
                    <button className="btn waves-effect" onClick={e=>{this.props.onSubmit() }}>Поиск</button>
                </div>
           </div>
        )
    }
}