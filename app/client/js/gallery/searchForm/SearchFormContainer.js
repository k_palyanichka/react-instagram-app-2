import React, { Component } from 'react';
import SearchFormLayout from './SearchFormLayout'

export default class extends Component {

    render() {
        return (
            <SearchFormLayout
                onSubmit={this.props.searchFormActions.addToTrackList.bind(this, {username: this.props.searchFormData.username})}
                onChange = {this.props.searchFormActions.setToTrackList}
                SearchValue ={this.props.searchFormData.username}
            />
        );
    }
}
