import * as TrackListActions from "../actions/TrackListActions";
import _ from "lodash";

const initialState = {
   trackList:[]
};

const deleteCheap = (state, id)=> {
    let filteredTrackList = _.filter(state.trackList, function(e) {
        return e.id !== id;
    });
    return {...state, trackList:filteredTrackList};
};


export default function TrackListReducer(state = initialState, action) {
    switch (action.type) {
        case  TrackListActions.SET_TRACK_LIST:
            return {...state, trackList: action.payload};

        case  TrackListActions.DELETE_CHEAP:
            return deleteCheap(state, action.payload);
    }

    return state
}