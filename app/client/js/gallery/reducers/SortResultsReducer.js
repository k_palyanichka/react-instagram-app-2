import * as SortResultsActions from "../actions/SortResultsActions";

const initialState = {
    sortKey:"",
};

export default function SortResultsReducer(state = initialState, action) {
     switch (action.type) {
         case SortResultsActions.SET_SORT_KEY:
             return {...state, sortKey:action.payload}
     }
     return state
}