import * as CheckAccActions from "../actions/CheckAccActions";
import _ from "lodash";

const initialState = {
    acc:[],
    open:true,
    password:"",
    username:""

};

export default function CheckAccReducer(state = initialState, action) {
    switch (action.type) {
        case  CheckAccActions.CHECK_ACC:
            if (action.payload==0){
                const open = true}
            return {...state, acc: action.payload};

        case  CheckAccActions.SET_PASSWORD:
              return {...state, password: action.payload};

         case  CheckAccActions.SET_USER:
              return {...state, username: action.payload};


        case  CheckAccActions.OPEN_DIALOG:
            console.log(action.payload)
            return {...state, open: true};

        case  CheckAccActions.CLOSE_DIALOG:
            return {...state, open: false};
    }
    return state
}
