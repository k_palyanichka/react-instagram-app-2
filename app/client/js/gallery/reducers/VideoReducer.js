import * as VideoActions from "../actions/VideoActions";
import _ from "lodash";

const initialState = {
    videoList:[],
    offset:0
};

const hideVideo = (state, video)=> {
    let filteredVideo = _.filter(state.videoList, function(e) {
        return e.media_id !== video;
    });
    return {...state, videoList:filteredVideo};
};

export default function VideoReducer(state = initialState, action) {
    switch (action.type) {
        case  VideoActions.SET_VIDEO_LIST:
            return {...state, videoList: state.videoList.concat(action.payload), offset:state.offset+1};

        case  VideoActions.HIDE_VIDEO:
            return hideVideo(state, action.payload);

        case VideoActions.UPDATE_VIDEO:
            return {...state, videoList:action.payload};

        case VideoActions.SORT_VIDEO_LIST:
            //let newVideoList = state.videoList.sort( function(o){return o[action.payload]});
            let newVideoList = _.orderBy(state.videoList, [action.payload], ['desc'])
            console.log(newVideoList)
            return {...state, videoList: newVideoList};
    }
    return state
}
