import * as SearchFormActions from "../actions/SearchFormActions";


const initialState = {
   username:[]
};

export default function TrackListReducer(state = initialState, action) {
    switch (action.type) {
        case  SearchFormActions.SET_TO_TRACKLIST:
            return {...state, username: action.payload};

        }

    return state
}