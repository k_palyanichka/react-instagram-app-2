import React, { Component } from 'react';

export default class extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: []
        }

    };


    render() {
        const {data} = this.props;

        return (
            <div >
                {data.map(item =>
                    <div key={item.media_id} className="col s9 m6 l4">
                        <div className="card-panel col s12" id ="videoCard">
                            <div className="col s12" id="video">
                                <video preload="false" className="responsive-video" id ="video" controls
                                       poster={item.video_image} src={item.video_url }>
                                    <source src={item.video_url} type="video/mp4"/>
                                </video>
                            </div>

                            <div id="created_at">
                                <span className="cr_txt">Опубликовано:</span>{ item.created_at }
                            </div>
                            <div className="user_name">
                                <span className="cr_txt">Пользователь: </span><a target="_blank"
                                                                                 href={"https://www.instagram.com/" + item.user_name + "/"}>{  item.user_name }</a>
                            </div>
                            <div className="views">
                                <span className="cr_txt">Просмотров: </span>
                                {  item.view_count } <i className="material-icons righth">visibility</i>
                            </div>
                            <div className="likes">
                                <span className="cr_txt">Понравилось: </span>
                                {item.like_count } <i className="material-icons righth">thumb_up</i>
                            </div>
                            <p></p>

                            <div className="action" id="actions">

                          <textarea placeholder="Описание с тегами" ref="tags" id="txtArea"
                                    rows={2} value={ item.text }
                                    onChange={this.props.change.bind(this, item.media_id)}/>
                                <p></p>

                                <div>
                                    <button className="btn-floating  waves-effect"
                                            type="submit"
                                            onClick={this.props.post.bind(this, item.media_id)}>
                                        <i className="material-icons right">publish</i>
                                    </button>
                                    <a href={item.video_url} download>
                                        <button className="btn-floating  waves-effect"><i
                                            className="material-icons right">send</i>
                                        </button>
                                    </a>

                                    <button className="btn-floating waves-effect red"
                                            onClick={this.props.hide.bind(this, item.media_id)}><i
                                        className="material-icons right">delete</i>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>)}
            </div>)
    }
}