import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import VideoGridLayout from './VideoGridLayout';
import LoadMoreBtnLayout from './LoadMoreBtnLayout';

export default class extends Component {

    componentDidMount = ()=> {
        this.props.videoActions.loadVideo(this.props.videoData.offset||0);

    };

    handlePost(media_id) {
        let data = this.props.videoData.videoList
        if (data.length <= 0) {
            var txt = data.filter(function (e) {
                return e.media_id == media_id
            });
        } else {
            var txt = data.filter(function (e) {
                return e.media_id == media_id
            });
        }
        let text =  txt[0].text;
        this.props.videoActions.postVideoFromList(media_id, text)

    };

    handleChange(media_id, e) {
        let data = this.props.videoData.videoList;
        let newVideo = $.each(data, function () {
            if (this.media_id == media_id) {
                this.text = e.target.value
            }
        });
        this.props.videoActions.updateVideo(newVideo); //We can reach actions this way
    };
    // TODO: реализовать получения статуса загрузки в виде сообщения
    //TODO: поисковые теги должны есздить с полосой прокрутки
    render() {
        let data = this.props.videoData.videoList
        //console.log("data from video grid container", data);

            return (
                <div  className="col s12 m9 l9 cards-container" id ="listVideos">
                    <div >
                        {
                            data && data.length   ?
                                <VideoGridLayout
                                    data={data}
                                    hide={this.props.videoActions.hideVideoFromList.bind(this)}
                                    post={this.handlePost.bind(this)}
                                    change={this.handleChange.bind(this)}
                                />
                                : null
                        }
                    </div>
                    {/*//TODO: кнопка всегда должна быть снизу видео !!!!*/}
                    <div className="raw">
                            <LoadMoreBtnLayout
                                onClick =  {()=> this.props.videoActions.loadVideo(this.props.videoData.offset||0)}
                            />
                    </div>
                </div>
            )
    }
}

