import React, { Component } from 'react';
import SortResultsLayout from './SortResultsLayout'

export default class extends Component {

    render() {
        return (
            <SortResultsLayout
                onClick={()=> this.props.videoActions.sortVideoList(this.props.sortResultsData.sortKey)}
                onChange = {(e)=>this.props.sortResultsActions.setSortKey(e.target.value)}
            />
        );
    }
}
