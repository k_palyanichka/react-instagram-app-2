import React, { Component } from 'react';

export default class extends Component {
    render() {
        const {onClick} = this.props;

        return (
            <div>
                <div className="input-field col s3">
                      <select onChange={(e)=> this.props.onChange(e)} className="browser-default">
                        <option value="" disabled selected>Выберете тип сортировки</option>
                        <option value="like_count">По лайкам</option>
                        <option value="view_count">По просмотрам</option>
                        <option value="date">По дате добавления</option>
                      </select>

                </div>
                <div className="col s1">
                    <button className="btn waves-effect" onClick={e=>this.props.onClick(e) }>Сортировать</button>
                </div>
           </div>
        )
    }
}