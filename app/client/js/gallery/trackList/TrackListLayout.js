import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Chip from 'material-ui/Chip';
injectTapEventPlugin();

export default class extends Component {
    render(){
        const {trackList} = this.props;
        return(
            <MuiThemeProvider>
                <div className="col s3">
                     <div  className="card-panel" id="chipsContainer">
                             <h3>Поисковые теги</h3>
                            <hr></hr>
                            {trackList.map(data=>
                        <Chip id ="chip"
                            key = {data.id}
                            onRequestDelete={this.props.delete.bind(this, data.id)}
                            onTouchTap={(e) => { e.preventDefault(); this.props.tap(data.name)}}>
                            {data.name}
                        </Chip>)}
                     </div>
                    </div>
            </MuiThemeProvider>
        )
    }
}