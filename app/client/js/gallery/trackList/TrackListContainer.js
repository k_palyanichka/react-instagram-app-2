import React, { Component } from 'react';
import { connect } from 'react-redux';
import TrackListLayout from './TrackListLayout';


export default class extends Component {
     componentDidMount = ()=> {
         this.props.trackListActions.loadTrackList()
     }

    render(){
        let trackList = this.props.trackListData.trackList
        return(
            <TrackListLayout
                trackList = {trackList}
                delete = {this.props.trackListActions.deleteCheapFromList.bind(this)}
                tap = {this.props.videoActions.updateVideoListByTap.bind(this)}
                />
        )
    }
}
