import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as GlobalActions from '../common/GlobalActions';
import * as VideoActions from './actions/VideoActions';
import * as TrackListActions from './actions/TrackListActions';
import * as CheckAccActions from './actions/CheckAccActions';
import * as SearchFormActions from './actions/SearchFormActions';
import * as SortResultsActions from './actions/SortResultsActions';

import VideoGridContainer from './videoGrid/VideoGridContainer';
import TrackListContainer from './trackList/TrackListContainer';
import InstaAccContainer from './instaAcc/InstaAccContainer';
import SearchFormContainer from './searchForm/SearchFormContainer';
import SortResultsContainer from  './sortingReults/SortResultsContainer'


const mapStateToProps = (state)=> {
    return {
        videoData: state.videoData,
        trackListData:state.trackListData,
        checkAccData:state.checkAccData,
        searchFormData:state.searchFormData,
        sortResultsData:state.sortResultsData,
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        globalActions: bindActionCreators(GlobalActions, dispatch),
        videoActions: bindActionCreators(VideoActions, dispatch),
        trackListActions: bindActionCreators(TrackListActions, dispatch),
        checkAccActions: bindActionCreators(CheckAccActions, dispatch),
        searchFormActions:bindActionCreators(SearchFormActions, dispatch),
        sortResultsActions:bindActionCreators(SortResultsActions, dispatch),
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export default class extends Component {


    render() {
        const {videoData, globalActions, videoActions, trackListActions, trackListData,
            checkAccActions, checkAccData, searchFormActions, searchFormData, sortResultsData, sortResultsActions} = this.props;


        return (
            <div className="galery-wrapper">

                <div className="row">
                    <SearchFormContainer
                        globalActions={globalActions}
                        searchFormActions={searchFormActions}
                        searchFormData={searchFormData}
                     />
                    <InstaAccContainer
                        globalActions={globalActions}
                        checkAccData={checkAccData}
                        checkAccActions ={checkAccActions}

                    />
                    <SortResultsContainer
                        globalActions={globalActions}
                        videoActions={videoActions}
                        sortResultsData = {sortResultsData}
                        sortResultsActions={sortResultsActions}
                    />
                </div>
                <div className="row">
                <div className="col s12">
                        <VideoGridContainer videoData={videoData}
                                            globalActions={globalActions}
                                            videoActions={videoActions}
                        />
                        <TrackListContainer  trackListData = {trackListData}
                                             globalActions={globalActions}
                                             trackListActions={trackListActions}
                                             videoActions={videoActions}
                        />
                </div>
            </div>
                </div>

        );
    }
}