import * as GlobalActions from '../../common/GlobalActions'
import axios from 'axios';


export const CHECK_ACC ='CHECK_ACC'
export const ADD_ACC ='ADD_ACC'
export const OPEN_DIALOG = 'OPEN_DIALOG'
export const CLOSE_DIALOG = 'CLOSE_DIALOG'
export const SET_PASSWORD = 'SET_PASSWORD'
export const SET_USER = 'SET_USER'

export function checkAccount(cnt_acc) {
    return {
        type: CHECK_ACC,
        payload: cnt_acc
    }
}

export function addAccount(acc_info) {
    return {
        type: ADD_ACC,
        payload: acc_info
    }
}

export function openDialog(){
    return{
        type:OPEN_DIALOG,
        payload: true
    }
}

export function closeDialog(){
    return{
        type:CLOSE_DIALOG,
        payload: true
    }
}

export function setPassword(pass){
    return{
        type:SET_PASSWORD,
        payload: pass
    }
}

export function setUser(user){
    return{
        type:SET_USER,
        payload: user
    }
}


export function addInstagramAcc(data) {
    return (dispatch) => {
        const url = '/api/add_acc'
        dispatch(GlobalActions.fetchStarted(url));
        dispatch(closeDialog())
        console.log(data)
         axios.post(url, data).then(res => {
            console.log(res);
            dispatch(closeDialog())
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err=>{
            console.log(err)
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}

export function checkInstagramAcc() {
    return (dispatch) => {
        const url = '/api/check_acc'
        dispatch(GlobalActions.fetchStarted(url));
        axios.get(url).then(res => {
            console.log(res);
            dispatch(checkAccount(res.data['cnt_acc']));
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err=>{
            console.log(err)
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}