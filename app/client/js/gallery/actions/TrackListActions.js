import * as GlobalActions from '../../common/GlobalActions'
import axios from 'axios';

export const SET_TRACK_LIST = "SET_TRACK_LIST"
export const DELETE_CHEAP = "DELETE_CHEAP"

export function setTrackList(trackList) {
    return {
        type: SET_TRACK_LIST,
        payload: trackList
    }
}


export function deleteCheap(id){
    return{
        type:DELETE_CHEAP,
        payload:id
    }
}

export function  deleteCheapFromList(id){
    return (dispatch) => {
        const url = '/api/delete_from_tracking?id='+id;
        dispatch(deleteCheap(id));
        dispatch(GlobalActions.fetchStarted(url));

        axios.get(url).then(res => {
            console.log(res);
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.error(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}
export function loadTrackList(){
    return (dispatch) => {
        const url = '/api/tracking_list';

        dispatch(GlobalActions.fetchStarted(url));

        axios.get(url).then(res => {
             const data = [];
            $.each(res.data, function (key, value) {
                data.push(value);
            });
            dispatch(setTrackList(data));
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.error(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}