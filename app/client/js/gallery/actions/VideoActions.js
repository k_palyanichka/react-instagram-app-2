import * as GlobalActions from '../../common/GlobalActions'
import axios from 'axios';

export const SET_VIDEO_LIST = 'SET_VIDEO_LIST';
export const HIDE_VIDEO ='HIDE_VIDEO';
export const POST_VIDEO ='POST_VIDEO';
export const UPDATE_VIDEO = 'UPDATE_VIDEO';
export const UPDATE_VIDEO_LIST = 'UPDATE_VIDEO_LIST';
export const SORT_VIDEO_LIST =  'SORT_VIDEO_LIST';

export  function sortVideoList(key){
    console.log("key to fun=>", key)
    return {
        type: SORT_VIDEO_LIST,
        payload: key
    }
}

export function setVideoList(videoList) {
    return {
        type: SET_VIDEO_LIST,
        payload: videoList
    }
}

export function updateVideoList(videoList) {
    return {
        type: UPDATE_VIDEO_LIST,
        payload: videoList
    }
}


export function hideVideo(video) {
     return {
         type: HIDE_VIDEO,
         payload: video
     }
}

export function postVideo(video) {
    return {
        type: POST_VIDEO,
        payload: video
    }
}

export function updateVideo(video) {
    return {
        type: UPDATE_VIDEO,
        payload: video
    }
}

export function updateVideoListByTap(name){
     return (dispatch) => {
         const url = '/api/update_content';
         dispatch(GlobalActions.fetchStarted(url));
         axios.post(url, {name:name} ).then(res => {
             console.log(res);
             dispatch(GlobalActions.fetchFinished(url));
         }).catch(err=>{
             console.log(err);
             dispatch(GlobalActions.fetchFinished(url));
         });
         dispatch(loadVideo());
     }
}

export function loadVideo(page){
    return (dispatch) => {
        console.log(page)
        console.log(page )
        const url = '/api/list_items?page=' + page;

        dispatch(GlobalActions.fetchStarted(url));
        
        axios.get(url).then(res => {
             const data = [];
             console.log(res);
            $.each(res.data, function (key, value) {
                data.push(value);

            });
            dispatch(setVideoList(data));
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.error(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}

export function  hideVideoFromList(media_id){
    return (dispatch) => {
        const url = "/hide/" + media_id;
        dispatch(hideVideo(media_id));
        dispatch(GlobalActions.fetchStarted(url));

        axios.get(url).then(res => {
            console.log(res);
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.error(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}

export function postVideoFromList(media_id, text) {
    return (dispatch) => {
        const url = '/api/publish'
        dispatch(postVideo(media_id));
        dispatch(hideVideoFromList(media_id));
        dispatch(GlobalActions.fetchStarted(url));
        axios.post(url, {media_id: media_id, tags: text}).then(res => {
            console.log(res);
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.log(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}


