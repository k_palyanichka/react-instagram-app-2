import * as GlobalActions from '../../common/GlobalActions'
import * as VideoActions from './VideoActions'
import * as TrackListActions from './TrackListActions'

import axios from 'axios';

export const SET_TO_TRACKLIST = 'SET_TO_TRACKLIST'

export function setToTrackList(username){
    return{
        type:SET_TO_TRACKLIST,
        payload: username
    }
}
export function addToTrackList(data){
    return(dispatch)=>{
        const url ='/api/tracking';
        console.log("data to search:", data)

        dispatch(GlobalActions.fetchStarted(url));
        axios.post(url, data).then(res => {
             console.log(res);
             dispatch(GlobalActions.fetchFinished(url));
         }).catch(err=>{
             console.log(err)
             dispatch(GlobalActions.fetchFinished(url));
        });
        dispatch(TrackListActions.loadTrackList());
        dispatch(VideoActions.updateVideoListByTap(data.username))
    }
}