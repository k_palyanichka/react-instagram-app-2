export  const SET_SORT_KEY = 'SET_SORT_KEY';

export function setSortKey(key) {
    return {
        type: SET_SORT_KEY,
        payload: key
    }
}
