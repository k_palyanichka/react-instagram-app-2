import React, { Component } from 'react';
import InstaAccLayout from './InstaAccLayout';
import FlatButton from 'material-ui/FlatButton';

export default class extends Component {
      componentDidMount = ()=> {
           this.props.checkAccActions.checkInstagramAcc();
      }
    render(){
        const actions = [
          <FlatButton
            label="Cancel"
            primary={true}
            onTouchTap={this.props.checkAccActions.closeDialog.bind(this)}
          />,
          <FlatButton
            label="Submit"
            primary={true}
            onTouchTap={e =>{
                this.props.checkAccActions.addInstagramAcc({username:this.props.checkAccData.username, password:this.props.checkAccData.password})}}
          />
        ];

        let cnt_acc = this.props.checkAccData.acc
        console.log("cnt_acc ->", cnt_acc)
        console.log(this.props.checkAccData.open)
        return(
            <div>
                {cnt_acc ==0 ?
                    <InstaAccLayout
                        openDialog = {this.props.checkAccActions.openDialog.bind(this)}
                        actions={actions}
                        open = {this.props.checkAccData.open}
                        setPass = {this.props.checkAccActions.setPassword}
                        setUser = {this.props.checkAccActions.setUser}
                        PassValue={this.props.checkAccData.password}
                        UserValue = {this.props.checkAccData.username}
                    />
                :null
                }
                </div>
        )
    }
}