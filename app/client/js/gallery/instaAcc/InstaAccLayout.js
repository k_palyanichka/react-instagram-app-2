import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Input from 'components/input/Input';

export default class extends Component {
    render(){
        return(
            <MuiThemeProvider>
            <div className="col s3" >
                <RaisedButton  primary={true} label="Добавить Instagram Account"
                               onTouchTap={this.props.openDialog.bind(this)} />
                <Dialog
                    title="Добавить аккаунт"
                    actions={this.props.actions}
                    modal={true}
                    open={this.props.open}
                >
                    <div className="col s5">
                        <Input
                          value={this.props.UserValue}
                          placeholder="Логин"
                          onChange={this.props.setUser}
                        />
                    </div>
                    <div className="col s5">
                        <Input
                          value={this.props.PassValue}
                          type="password"
                          placeholder="Пароль"
                          onChange={this.props.setPass}
                         />
                    </div>
                </Dialog>
            </div>
            </MuiThemeProvider>
        )
    }
}