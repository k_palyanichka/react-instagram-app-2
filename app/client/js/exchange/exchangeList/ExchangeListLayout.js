import React, { Component } from 'react';
//todo добавить group_name,  price
export default class extends Component {
    render(){
        const {offersList} = this.props;
        return(
            <table class="striped bordered">
              <thead>
                 <tr>
                     <th>Тематика</th><th>Название</th><th>Подписчиков/просмотров/лайков</th><th>Менеджер</th><th>Условия</th>
                 </tr>
              </thead>
              <tbody>
              {offersList.map(offer=>
                 <tr>
                     <td>{offer.subject}</td>
                     <td>
                         <a target="_blank" href={offer.link}>{offer.name}</a>
                     </td>
                     <td>{offer.followers}/{offer.views}/{offer.likes}</td>
                     <td>
                         <a target="_blank" href={offer.manager_link}>{offer.manager}</a>
                     </td>
                     <td>{offer.price}</td>
                 </tr>

              )}
              </tbody>
            </table>
        )
    }
}