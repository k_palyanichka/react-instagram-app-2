import React, { Component } from 'react';
import ExchangeListLayout from './ExchangeListLayout'

export default class extends Component {
    componentDidMount(){
      this.props.exchangeActions.loadExchangeList()
    };
    render() {
        return (
            <ExchangeListLayout
                offersList = {this.props.exchangeData.offersList}
            />
        )
    }
}