import * as GlobalActions from '../../common/GlobalActions'
import addOffer from './ExchangeActions';

export const SET_SUBJECT = 'SET_SUBJECT'
export const SET_LINK = 'SET_LINK'
export const SET_FOLLOWERS = 'SET_FOLLOWERS'
export const SET_LIKES = 'SET_LIKES'
export const SET_VIEWS = 'SET_VIEWS'
export const SET_MANAGER = 'SET_MANAGER'
export const SET_MANAGER_LINK = 'SET_MANAGER_LINK'
export const SET_TIME_POST = 'SET_TIME_POST'
export const SET_DESCRIPTION = 'SET_DESCRIPTION'
export const OPEN_DIALOG = 'OPEN_DIALOG'
export const CLOSE_DIALOG = 'CLOSE_DIALOG'
export const SET_NAME = "SET_NAME"
export const SET_PRICE = "SET_PRICE"

export function setSubject(subject){
    return{
        type:SET_SUBJECT,
        payload: subject
    }
}
export function setLink(link){
    return{
        type:SET_LINK,
        payload: link
    }
}
export function setFollowers(followers){
    return{
        type:SET_FOLLOWERS,
        payload: followers
    }
}
export function setLikes(likes){
    return{
        type:SET_LIKES,
        payload: likes
    }
}
export function setViews(views){
    return{
        type:SET_VIEWS,
        payload: views
    }
}
export function setManager(manager){
    return{
        type:SET_MANAGER,
        payload: manager
    }
}
export function setManagerLink(managerLink){
    return{
        type:SET_MANAGER_LINK,
        payload: managerLink
    }
}
export function setTimePost(timePost){
    return{
        type:SET_TIME_POST,
        payload: timePost
    }
}
export function setDescription(description){
    return{
        type:SET_DESCRIPTION,
        payload: description.target.value
    }
}
export function setName(name){
    return{
        type:SET_NAME,
        payload: name
    }
}

export function setPrice(price){
    return{
        type:SET_PRICE,
        payload: price
    }
}


export function openDialog(){
    return{
        type:OPEN_DIALOG,
        payload: true
    }
}

export function closeDialog(){
    return{
        type:CLOSE_DIALOG,
        payload: true
    }
}



