import * as GlobalActions from '../../common/GlobalActions'
import {closeDialog} from './SetFormActions';

import axios from 'axios';


export const SET_EXCHANGELIST = 'SET_EXCHANGELIST'
export const ADD_TO_EXCHANGELIST = 'ADD_TO_EXCHANGELIST'

export function setExchangeList(offersList){
    return{
        type:SET_EXCHANGELIST,
        payload: offersList
    }
}

export function addToExchangeList(offer){
    return{
        type:ADD_TO_EXCHANGELIST,
        payload: offer
    }
}

//todo  окно дожно закрываться после submit
export function addOffer(offer){
    return(dispatch)=>{
        const url = '/api/exchange/addoffer';
        dispatch(addToExchangeList(offer))
        dispatch(GlobalActions.fetchStarted(url));
        dispatch(closeDialog());
        axios.post(url, offer).then(res=>{
            console.log("adding offer")
            console.log(res)
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err=>{
            console.log(err)
        })
    }
}

export function loadExchangeList(){
    return (dispatch) => {
        const url = '/api/exchange/getoffers';

        dispatch(GlobalActions.fetchStarted(url));

        axios.get(url).then(res => {
             const data = [];
            $.each(res.data, function (key, value) {
                data.push(value);
            });
            dispatch(setExchangeList(data));
            dispatch(GlobalActions.fetchFinished(url));
        }).catch(err => {
            console.error(err);
            dispatch(GlobalActions.fetchFinished(url));
        });
    }
}