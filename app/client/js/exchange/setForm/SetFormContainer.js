import React, { Component } from 'react';
import SetFormLayout from './SetFormLayout'
import FlatButton from 'material-ui/FlatButton';
export default class extends Component {

    render() {
        const actions = [
          <FlatButton
            label="Close"
            primary={true}
            onTouchTap={this.props.setFormActions.closeDialog.bind(this)}
          />,
          <FlatButton
            label="Submit"
            primary={true}
            onTouchTap={e =>{
                this.props.exchangeActions.addOffer({data:this.props.setFormData})}}
          />
        ];

        return (
            <SetFormLayout
                //onSubmit={this.props.setFormActions.addToTrackList.bind(this, {username: this.props.searchFormData.username})}
                //onchange = {this.props.searchFormActions.setToTrackList.bind(this)}
                setSubject={this.props.setFormActions.setSubject}
                setLink={this.props.setFormActions.setLink}
                setFollowers={this.props.setFormActions.setFollowers}
                setLikes={this.props.setFormActions.setLikes}
                setViews={this.props.setFormActions.setViews}
                setManager={this.props.setFormActions.setManager}
                setManagerLink={this.props.setFormActions.setManagerLink}
                setTimePost={this.props.setFormActions.setTimePost}
                setDescription={this.props.setFormActions.setDescription}
                setName={this.props.setFormActions.setName}
                setPrice={this.props.setFormActions.setPrice}

                SubjectValue={this.props.setFormData.subject}
                LinkValue={this.props.setFormData.link}
                FollowersValue={this.props.setFormData.followers}
                LikesValue={this.props.setFormData.likes}
                ViewsValue={this.props.setFormData.views}
                ManagerValue={this.props.setFormData.manager}
                ManagerLinkValue={this.props.setFormData.manager_link}
                TimePostValue={this.props.setFormData.time_post}
                DescriptionValue={this.props.setFormData.description}
                NameValue={this.props.setFormData.name}
                PriceValue={this.props.setFormData.price}

                open = {this.props.setFormData.open}
                openDialog = {this.props.setFormActions.openDialog.bind(this)}
                actions = {actions}
            />
        );
    }
}
