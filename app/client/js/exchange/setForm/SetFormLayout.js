import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Input from 'components/input/Input';

export default class extends Component {
    render() {
        return (
            <MuiThemeProvider>
            <div className="col s3" >
                <RaisedButton  primary={true} label="Добавить предложения рекламы"
                               onTouchTap={this.props.openDialog.bind(this)}
                    />
                <Dialog
                    title="Добавить предложения рекламы"
                    actions={this.props.actions}
                    modal={true}
                    open={this.props.open}
                >

                <div className="row">
                    <div className="col s4">
                    <Input
                        placeholder="Тематика"
                        value={this.props.SubjectValue}
                        onChange={this.props.setSubject}
                    />
                    </div>
                     <div className="col s4">
                    <Input
                        placeholder="Наименование"
                        value={this.props.NameValue}
                        onChange={this.props.setName}
                    />
                    </div>
                    <div className="col s4">
                    <Input
                        placeholder="Ссылка"
                        value={this.props.LinkValue}
                        onChange={this.props.setLink}
                    />
                    </div>
                </div>
                <div className="row">
                    <div className="col s4">
                    <Input
                        type="number"
                        placeholder="Подписчики"
                        value={this.props.FollowersValue}
                        onChange= {this.props.setFollowers}
                    />
                    </div>
                    <div className="col s4">
                    <Input
                        type="number"
                        placeholder="Лайки"
                        value={this.props.LikesValue}
                        onChange= {this.props.setLikes}
                    />
                    </div>
                    <div className="col s4">
                    <Input
                        type="number"
                        placeholder="Просмотры"
                        value={this.props.ViewsValue}
                        onChange= {this.props.setViews}
                    />
                    </div>
                </div>
                <div className="row">
                    <div className="col s4">
                    <Input
                        placeholder="Менеджер"
                        value={this.props.ManagerValue}
                        onChange= {this.props.setManager}
                    />
                    </div>
                    <div className="col s4">
                    <Input
                        placeholder="Ссылка на профиль в соц сетях менеджера"
                        value={this.props.ManagerLinkValue}
                        onChange= {this.props.setManagerLink}
                    />
                    </div>
                    <div className="col s4">
                    <Input
                        placeholder="Условия"
                        value={this.props.PriceValue}
                        onChange= {this.props.setPrice}
                    />
                    </div>
                </div>
                <div className="row">
                <div className="col s10">
                    <textarea className="materialize-textarea"
                        name="description"
                        type="text"
                        placeholder="Описание"
                        rows={2}
                        value={this.props.DescriptionValue}
                        onChange= {this.props.setDescription}
                    />
                </div>
                </div>
                </Dialog>
           </div>
            </MuiThemeProvider>
        )
    }
}