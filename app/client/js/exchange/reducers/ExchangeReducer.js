import * as ExchangeActions from "../actions/ExchangeActions";


const initialState = {
   offersList:[]
};

export default function ExchangeListReducer(state = initialState, action) {
    switch (action.type) {
        case  ExchangeActions.SET_EXCHANGELIST:
            return {...state, offersList: action.payload};

        case ExchangeActions.ADD_TO_EXCHANGELIST:
            let newOffersList= _.concat(state.offersList, action.payload.data)
            return{...state, offersList:newOffersList}
    }


    return state
}