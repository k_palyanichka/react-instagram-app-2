import * as SetFormActions from "../actions/SetFormActions";


const initialState = {
    subject:"",
    name:"",
    price:"",
    link:"",
    followers:"",
    likes:"",
    views:"",
    manager:"",
    manager_link:"",
    cr_date:"",
    description:"",
    open:true

};

export default function SetFormReducer(state = initialState, action) {
    switch (action.type) {
        case  SetFormActions.SET_SUBJECT:
            return {...state, subject: action.payload};

        case SetFormActions.SET_LINK:
            return{...state, link:action.payload}

        case  SetFormActions.SET_FOLLOWERS:
            return {...state, followers: action.payload};

        case SetFormActions.SET_LIKES:
            return{...state, likes:action.payload}

        case  SetFormActions.SET_VIEWS:
            return {...state, views: action.payload};

        case SetFormActions.SET_MANAGER:
            return{...state, manager:action.payload}

        case  SetFormActions.SET_MANAGER_LINK:
            return {...state, manager_link: action.payload};

        case SetFormActions.SET_DESCRIPTION:
            return{...state, description:action.payload};

        case SetFormActions.SET_NAME:
            return{...state, name:action.payload} ;

        case SetFormActions.SET_PRICE:
            return{...state, price:action.payload} ;


        case  SetFormActions.OPEN_DIALOG:
            console.log(action.payload)
            return {...state, open: true};

        case  SetFormActions.CLOSE_DIALOG:
            return {...state, open: false};
    }


    return state
}