import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as GlobalActions from '../common/GlobalActions';
import * as ExchangeActions from './actions/ExchangeActions';
import * as SetFormActions from './actions/SetFormActions';

import SetFormContainer from './setForm/SetFormContainer';
import ExchangeListContainer from './exchangeList/ExchangeListContainer';

const mapStateToProps = (state)=> {
    return {
        exchangeData: state.exchangeData,
        setFormData:state.setFormData
       }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        globalActions: bindActionCreators(GlobalActions, dispatch),
        exchangeActions:bindActionCreators(ExchangeActions, dispatch),
        setFormActions:bindActionCreators(SetFormActions, dispatch),
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export default class extends Component {
    render() {
        const {globalActions, exchangeActions, exchangeData, setFormActions, setFormData} = this.props;


        return (
            <div className="exchange-wrapper">
                <div className="row">
                    <SetFormContainer
                        globalActions={globalActions}
                        setFormActions={setFormActions}
                        setFormData={setFormData}
                        exchangeActions={exchangeActions}
                    />
                </div>
                <div className="row">
                    <ExchangeListContainer
                         globalActions={globalActions}
                         exchangeActions={exchangeActions}
                         exchangeData={exchangeData}
                    />

            </div>
                </div>

        );
    }
}