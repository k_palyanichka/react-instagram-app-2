/**
 * Created by kpalyanichka on 31.03.2017.
 */

import React, {Component} from 'react';

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    getPages = () => {
        const {totalPages} = this.props;

        let pages = [];
        for (let i = 1; i <= totalPages; i++) {
            pages.push(i);
        }

        return pages.length && pages.map((page) => {
                return <div>{page}</div>
            })
    }




    render() {
        const {value, placeholder} = this.props;

        let response = {
            list: [],
            totalPageCount: 5
        };


        return (
            <div className="pagination-container">
                <div>First</div>
                <div>Previous</div>
                {this.getPages()}
                <div>Next</div>
                <div>Last</div>
            </div>
        );
    }
}