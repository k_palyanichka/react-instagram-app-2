/**
 * Created by kpalyanichka on 31.03.2017.
 */

import React, {Component} from 'react';

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    onChange = (evt) => {
        this.props.onChange(evt.target.value);
    };

    render() {
        const {value, placeholder, type} = this.props;

        return (
            <input onChange={this.onChange}
                   value={value || ''}
                   placeholder={placeholder || ''}
                   type={type||"text"}
            />
        );
    }
}