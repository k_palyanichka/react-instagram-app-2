
import React from "react";
import { render } from "react-dom";
import { Router, Route, IndexRoute, Link, browserHistory } from "react-router";
import { Provider } from "react-redux";
import { syncHistoryWithStore } from "react-router-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { IntlProvider } from 'react-intl';
import rootReducer from "./js/reducers/CombineReducers";
import App from "./js/App";
import HomeContainer from "./js/home/HomeContainer";
import GalleryContainer from "./js/gallery/GalleryContainer";
import ExchangeContainer from "./js/exchange/ExchangeContainer";
import NotFound from "./js/common/404";
import { composeWithDevTools } from 'redux-devtools-extension';

import "babel-polyfill";
require("./less/style.less");


const INITIAL_STATE = {
    globalData: {
        fetchesInProgress: []
    },
    videoData: {
        videoList: [],
        offset:0
    },
    trackListData:{
        trackList:[]
    },
    checkAccData:{
        acc:[],
        open:false,
        password:"",
        username:""
    },
    searchFormData:{
        username:""
    },
    setFormData:{
        subject:"",
        link:"",
        followers:"",
        likes:"",
        views:"",
        manager:"",
        manager_link:"",
        time_post:"",
        description:"",
        open:false
    },
    exchangeData:{
        offersList:[]
    },
    sortResultsData:{
        sortKey:""
    }
};

const store = createStore(rootReducer, INITIAL_STATE, composeWithDevTools(applyMiddleware(thunk)));
const history = syncHistoryWithStore(browserHistory, store);

render(
    <Provider store={store}>
        <IntlProvider locale="en">
            <Router history={history}>

                <Route path="/" component={App}>
                    <IndexRoute component={GalleryContainer}/>
                    <Route path="/" component={GalleryContainer}/>
                    <Route path="exchange" component={ExchangeContainer}/>
                    <Route path="*" component={NotFound}/>
                </Route>
            </Router>
        </IntlProvider>
    </Provider>,
    document.getElementById('root')
);