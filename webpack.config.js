var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    'babel-polyfill',
    './app/client/index'
  ],
  output: {
    path: path.join(__dirname, 'app', 'static'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin('bundle.css'),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
  module: {
    rules: [
      {
        loaders: ['babel-loader'],
        include: [
          path.resolve("./app/client")
        ],
        test: /\.js$/
      },
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        include: [
          path.resolve("./app/client")
        ]
      },
      {
        test: /\.css$/,
        include: [
          path.resolve("./app/client")
        ],
        loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})
      },
      {
        test: /\.less$/,
        include: [
          path.resolve("./app/client")
        ],
        loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader!less-loader"})
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
      {
        test: /\.(gif|png|jpg|)$/,
        include: [
          path.resolve("./app/client")
        ],
        loader: 'file-loader'
      }

    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css'],
    modules: [
      path.resolve("./app/client"),
      'node_modules'
    ]
  }
};